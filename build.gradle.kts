val logging_interceptor_version = "3.8.0"
val retrofit_version = "2.9.0"
val junit_version = "5.10.0"
val mock_web_server_version = "4.2.1"
val coroutines_version = "1.6.2"

plugins {
    kotlin("jvm") version "1.8.10"
    `maven-publish`
    `java-library`
}

group = "com.github.kotlintelegrambot"
version = "by-ru.zheg-edited"

java.sourceCompatibility = JavaVersion.VERSION_17

repositories {
    mavenCentral()
}

java {
    withJavadocJar()
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("kotlin-telegram-bot") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            name = "kotlin-telegram-bot"
            url = uri("https://gitlab.com/api/v4/projects/52077849/packages/maven")
            credentials(PasswordCredentials::class) {
                username = "gitlab-ci-token"
                password = System.getenv("CI_JOB_TOKEN")
//                    findProperty("gitLabPrivateToken") as String? // the variable resides in $GRADLE_USER_HOME/gradle.properties
            }
            authentication {
                create("basic", BasicAuthentication::class)
            }
        }
    }
}

dependencies {
    implementation("com.google.code.gson:gson:2.10")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutines_version")
    implementation("com.squareup.okhttp3:logging-interceptor:$logging_interceptor_version")
    implementation("com.squareup.retrofit2:retrofit:$retrofit_version")
    implementation("com.squareup.retrofit2:converter-gson:$retrofit_version")
    // Client engine
    implementation("com.squareup.okhttp3:okhttp:4.10.0")

    implementation("io.github.microutils:kotlin-logging-jvm:2.1.20")

    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:$junit_version")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testImplementation("org.junit.jupiter:junit-jupiter-params:$junit_version")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("io.mockk:mockk:1.10.2")
    testImplementation("com.squareup.okhttp3:mockwebserver:$mock_web_server_version")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutines_version")
}

tasks.test {
    useJUnitPlatform()
}
