package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyDice
import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class DiceHandlerTest {

    private val handleDiceMock = mockk<HandleDice>(relaxed = true)

    private val sut = DiceHandler(handleDiceMock)

    @Test
    fun `checkUpdate returns false when there is no message`() {
        val anyUpdateWithNoMessage = anyUpdate(message = null)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoMessage)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when there is no dice`() {
        val anyUpdateWithNoDice = anyUpdate(message = anyMessage(dice = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoDice)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is dice`() {
        val anyUpdateWithDice = anyUpdate(message = anyMessage(dice = anyDice()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithDice)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `dice is properly dispatched to the handler function`() = runTest {
        val anyDice = anyDice()
        val anyMessageWithDice = anyMessage(dice = anyDice)
        val anyUpdateWithDice = anyUpdate(message = anyMessageWithDice)

        sut.handleUpdate(anyUpdateWithDice)

        val expectedDiceHandlerEnv = DiceHandlerEnvironment(
            anyUpdateWithDice,
            anyMessageWithDice,
            anyDice
        )
        coVerify { handleDiceMock.invoke(expectedDiceHandlerEnv) }
    }
}
