package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyContact
import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class ContactHandlerTest {

    private val handleContactMock = mockk<HandleContact>(relaxed = true)

    private val sut = ContactHandler(handleContactMock)

    @Test
    fun `checkUpdate returns false when there is no contact`() {
        val anyUpdateWithNoContact = anyUpdate(message = anyMessage(contact = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoContact)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is contact`() {
        val anyUpdateWithContact = anyUpdate(message = anyMessage(contact = anyContact()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithContact)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `contact is properly dispatched to the handler function`() = runTest {
        val anyContact = anyContact()
        val anyMessageWithContact = anyMessage(contact = anyContact)
        val anyUpdateWithContact = anyUpdate(message = anyMessageWithContact)

        sut.handleUpdate(anyUpdateWithContact)

        val expectedCommandHandlerEnv = ContactHandlerEnvironment(
            anyUpdateWithContact,
            anyMessageWithContact,
            anyContact
        )
        coVerify { handleContactMock.invoke(expectedCommandHandlerEnv) }
    }
}
