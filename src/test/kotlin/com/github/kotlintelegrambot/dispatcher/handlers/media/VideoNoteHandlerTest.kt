package com.github.kotlintelegrambot.dispatcher.handlers.media

import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.anyVideoNote
import com.github.kotlintelegrambot.dispatcher.handlers.HandleVideoNote
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class VideoNoteHandlerTest {

    private val handleVideoNoteMock = mockk<HandleVideoNote>(relaxed = true)

    private val sut = VideoNoteHandler(handleVideoNoteMock)

    @Test
    fun `checkUpdate returns false when there is no video note`() {
        val anyUpdateWithNoVideoNote = anyUpdate(message = anyMessage(videoNote = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoVideoNote)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is video note`() {
        val anyUpdateWithVideoNote = anyUpdate(message = anyMessage(videoNote = anyVideoNote()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithVideoNote)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `video note is properly dispatched to the handler function`() = runTest {
        val anyVideoNote = anyVideoNote()
        val anyMessageWithVideoNote = anyMessage(videoNote = anyVideoNote)
        val anyUpdateWithVideoNote = anyUpdate(message = anyMessageWithVideoNote)

        sut.handleUpdate(anyUpdateWithVideoNote)

        val expectedVideoNoteHandlerEnv = MediaHandlerEnvironment(
            anyUpdateWithVideoNote,
            anyMessageWithVideoNote,
            anyVideoNote
        )
        coVerify { handleVideoNoteMock.invoke(expectedVideoNoteHandlerEnv) }
    }
}
