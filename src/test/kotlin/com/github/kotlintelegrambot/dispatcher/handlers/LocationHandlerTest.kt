package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyLocation
import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class LocationHandlerTest {

    private val handleLocationMock = mockk<HandleLocation>(relaxed = true)

    private val sut = LocationHandler(handleLocationMock)

    @Test
    fun `checkUpdate returns false when there is no location`() {
        val anyUpdateWithNoLocation = anyUpdate(message = anyMessage(location = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoLocation)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is location`() {
        val anyUpdateWithLocation = anyUpdate(message = anyMessage(location = anyLocation()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithLocation)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `location is properly dispatched to the handler function`() = runTest {
        val anyLocation = anyLocation()
        val anyMessageWithLocation = anyMessage(location = anyLocation)
        val anyUpdateWithLocation = anyUpdate(message = anyMessageWithLocation)

        sut.handleUpdate(anyUpdateWithLocation)

        val expectedLocationHandlerEnv = LocationHandlerEnvironment(
            anyUpdateWithLocation,
            anyMessageWithLocation,
            anyLocation
        )
        coVerify { handleLocationMock.invoke(expectedLocationHandlerEnv) }
    }
}
