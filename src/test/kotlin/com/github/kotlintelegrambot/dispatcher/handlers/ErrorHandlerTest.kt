package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.errors.TelegramError
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class ErrorHandlerTest {

    private val handleErrorMock = mockk<HandleError>(relaxed = true)

    private val sut = ErrorHandler(handleErrorMock)

    @Test
    fun `error is properly dispatched to handler function`() {
        val anyTelegramError = mockk<TelegramError>()

        sut.invoke(anyTelegramError)

        val expectedErrorHandlerEnv = ErrorHandlerEnvironment(anyTelegramError)
        verify { handleErrorMock.invoke(expectedErrorHandlerEnv) }
    }
}
