package com.github.kotlintelegrambot.dispatcher.handlers.media

import com.github.kotlintelegrambot.anyDocument
import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.dispatcher.handlers.HandleDocument
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class DocumentHandlerTest {

    private val handleDocumentMock = mockk<HandleDocument>(relaxed = true)

    private val sut = DocumentHandler(handleDocumentMock)

    @Test
    fun `checkUpdate returns false when there is no document`() {
        val anyUpdateWithNoDocument = anyUpdate(message = anyMessage(document = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoDocument)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is document`() {
        val anyUpdateWithDocument = anyUpdate(message = anyMessage(document = anyDocument()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithDocument)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `document is properly dispatched to the handler function`() = runTest {
        val anyDocument = anyDocument()
        val anyMessageWithDocument = anyMessage(document = anyDocument)
        val anyUpdateWithDocument = anyUpdate(message = anyMessageWithDocument)

        sut.handleUpdate(anyUpdateWithDocument)

        val expectedDocumentHandlerEnv = MediaHandlerEnvironment(
            anyUpdateWithDocument,
            anyMessageWithDocument,
            anyDocument
        )
        coVerify { handleDocumentMock.invoke(expectedDocumentHandlerEnv) }
    }
}
