package com.github.kotlintelegrambot.dispatcher.handlers.media

import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.anyVoice
import com.github.kotlintelegrambot.dispatcher.handlers.HandleVoice
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class VoiceHandlerTest {

    private val handleVoiceMock = mockk<HandleVoice>(relaxed = true)

    private val sut = VoiceHandler(handleVoiceMock)

    @Test
    fun `checkUpdate returns false when there is no voice`() {
        val anyUpdateWithNoVoice = anyUpdate(message = anyMessage(voice = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoVoice)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is voice`() {
        val anyUpdateWithVoice = anyUpdate(message = anyMessage(voice = anyVoice()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithVoice)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `voice is properly dispatched to the handler function`() = runTest {
        val anyVoice = anyVoice()
        val anyMessageWithVoice = anyMessage(voice = anyVoice)
        val anyUpdateWithVoice = anyUpdate(message = anyMessageWithVoice)

        sut.handleUpdate(anyUpdateWithVoice)

        val expectedVoiceHandlerEnv = MediaHandlerEnvironment(
            anyUpdateWithVoice,
            anyMessageWithVoice,
            anyVoice
        )
        coVerify { handleVoiceMock.invoke(expectedVoiceHandlerEnv) }
    }
}
