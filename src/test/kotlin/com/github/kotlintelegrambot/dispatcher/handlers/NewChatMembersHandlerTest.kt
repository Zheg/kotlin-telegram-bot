package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.anyUser
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class NewChatMembersHandlerTest {

    private val handleNewChatMembersHandlerMock = mockk<HandleNewChatMembers>(relaxed = true)

    private val sut = NewChatMembersHandler(handleNewChatMembersHandlerMock)

    @Test
    fun `checkUpdate returns false when there is no message`() {
        val anyUpdateWithNoMessage = anyUpdate(message = null)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoMessage)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when there are no new chat members`() {
        val anyUpdateWithNoChatMembers = anyUpdate(message = anyMessage(newChatMembers = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoChatMembers)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when new chat members are empty`() {
        val anyUpdateWithEmptyNewChatMembers = anyUpdate(message = anyMessage(newChatMembers = emptyList()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithEmptyNewChatMembers)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when new chat members are not empty`() {
        val anyUpdateWithNewChatMembers = anyUpdate(message = anyMessage(newChatMembers = listOf(anyUser())))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNewChatMembers)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `new chat members are properly dispatched to the handler function`() = runTest {
        val anyNewChatMembers = listOf(anyUser())
        val anyMessageWithNewChatMembers = anyMessage(newChatMembers = anyNewChatMembers)
        val anyUpdateWithNewChatMembers = anyUpdate(message = anyMessageWithNewChatMembers)

        sut.handleUpdate(anyUpdateWithNewChatMembers)

        val expectedNewChatMembersHandlerEnv = NewChatMembersHandlerEnvironment(
            anyUpdateWithNewChatMembers,
            anyMessageWithNewChatMembers,
            anyNewChatMembers
        )
        coVerify { handleNewChatMembersHandlerMock.invoke(expectedNewChatMembersHandlerEnv) }
    }
}
