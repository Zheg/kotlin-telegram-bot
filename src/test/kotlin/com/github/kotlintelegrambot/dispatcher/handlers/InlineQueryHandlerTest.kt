package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyInlineQuery
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class InlineQueryHandlerTest {

    private val handleInlineQueryMock = mockk<HandleInlineQuery>(relaxed = true)

    private val sut = InlineQueryHandler(handleInlineQueryMock)

    @Test
    fun `checkUpdate returns true when there is inline query`() {
        val anyUpdateWithInlineQuery = anyUpdate(inlineQuery = anyInlineQuery())

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithInlineQuery)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when there is no inline query`() {
        val anyUpdateWithoutInlineQuery = anyUpdate(inlineQuery = null)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithoutInlineQuery)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `inline query is properly dispatched to the handler function`() = runTest {
        val anyInlineQuery = anyInlineQuery()
        val anyUpdateWithInlineQuery = anyUpdate(inlineQuery = anyInlineQuery)

        sut.handleUpdate(anyUpdateWithInlineQuery)

        val expectedInlineQueryHandlerEnv = InlineQueryHandlerEnvironment(
            anyUpdateWithInlineQuery,
            anyInlineQuery
        )
        coVerify { handleInlineQueryMock.invoke(expectedInlineQueryHandlerEnv) }
    }
}
