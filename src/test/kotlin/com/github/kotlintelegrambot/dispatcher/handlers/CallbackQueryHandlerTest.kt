package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.anyCallbackQuery
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.network.TelegramClient
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class CallbackQueryHandlerTest {

    private val handleCallbackQueryMock = mockk<HandleCallbackQuery>().also {
        coEvery { it.invoke(any()) } returns CallbackQueryResponse(
            text = ANY_CALLBACK_ANSWER_TEXT,
            showAlert = CALLBACK_ANSWER_SHOW_ALERT,
            url = ANY_ANSWER_CALLBACK_URL,
            cacheTime = ANY_CALLBACK_ANSWER_CACHE_TIME,
        )
    }

    private val telegramClientMock = mockk<TelegramClient>(relaxed = true)

    @Test
    fun `checkUpdate returns false when there is no callback query`() {
        val anyUpdateWithoutCallbackQuery = anyUpdate(callbackQuery = null)
        val sut =
            CallbackQueryHandler(telegramClient = telegramClientMock, handleCallbackQuery = handleCallbackQueryMock)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithoutCallbackQuery)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is callback query and no callback data to match`() {
        val anyUpdateWithCallbackQuery = anyUpdate(callbackQuery = anyCallbackQuery())
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = null,
            handleCallbackQuery = handleCallbackQueryMock
        )

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithCallbackQuery)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when there is callback query and its data doesn't match the data to match`() {
        val anyUpdateWithCallbackQuery = anyUpdate(
            callbackQuery = anyCallbackQuery(data = ANY_CALLBACK_QUERY_DATA)
        )
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = ANY_OTHER_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock
        )

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithCallbackQuery)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is callback query and its data is equal to the data to match`() {
        val anyUpdateWithCallbackQuery = anyUpdate(
            callbackQuery = anyCallbackQuery(data = ANY_CALLBACK_QUERY_DATA)
        )
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = ANY_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock
        )

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithCallbackQuery)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns false when there is callback query and its data contains the data to match but not equals`() {
        val anyUpdateWithCallbackQuery = anyUpdate(
            callbackQuery = anyCallbackQuery(data = ANY_CALLBACK_QUERY_DATA)
        )
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = ANY_DATA_CONTAINED_IN_ANY_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock
        )

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithCallbackQuery)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is callback query and its data equals`() {
        val anyUpdateWithCallbackQuery = anyUpdate(
            callbackQuery = anyCallbackQuery(data = ANY_CALLBACK_QUERY_DATA)
        )
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = ANY_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock
        )

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithCallbackQuery)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `callbackQuery is properly dispatched to the handler function`() = runTest {
        val anyCallbackQuery = anyCallbackQuery(data = ANY_CALLBACK_QUERY_DATA)
        val botMock = mockk<Bot>(relaxed = true)
        val anyUpdateWithCallbackQuery = anyUpdate(callbackQuery = anyCallbackQuery)
        val sut = CallbackQueryHandler(
            telegramClient = telegramClientMock,
            callbackData = ANY_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock
        )

        sut.handleUpdate(anyUpdateWithCallbackQuery)

        val expectedCallbackQueryHandlerEnvironment = CallbackQueryHandlerEnvironment(
            anyUpdateWithCallbackQuery,
            anyCallbackQuery,
        )
        coVerify { handleCallbackQueryMock.invoke(expectedCallbackQueryHandlerEnvironment) }
    }

    @Test
    fun `callback query is answered when callbackQuery is dispatched to the handler function`() = runTest {
        val clientMock = mockk<TelegramClient>(relaxed = true)
        val anyUpdateWithCallbackQuery = anyUpdate(
            callbackQuery = anyCallbackQuery(
                id = ANY_CALLBACK_QUERY_ID,
                data = ANY_CALLBACK_QUERY_DATA
            )
        )
        val sut = CallbackQueryHandler(
            telegramClient = clientMock,
            callbackData = ANY_CALLBACK_QUERY_DATA,
            handleCallbackQuery = handleCallbackQueryMock,
        )

        sut.handleUpdate(anyUpdateWithCallbackQuery)

        coVerify {
            clientMock.answerCallbackQuery(
                callbackQueryId = ANY_CALLBACK_QUERY_ID,
                text = ANY_CALLBACK_ANSWER_TEXT,
                showAlert = CALLBACK_ANSWER_SHOW_ALERT,
                url = ANY_ANSWER_CALLBACK_URL,
                cacheTime = ANY_CALLBACK_ANSWER_CACHE_TIME
            )
        }
    }

    private companion object {
        const val ANY_CALLBACK_QUERY_DATA = "yeheeee"
        const val ANY_OTHER_CALLBACK_QUERY_DATA = "yiiiiiihiii"
        const val ANY_DATA_CONTAINED_IN_ANY_CALLBACK_QUERY_DATA = "yeh"
        const val ANY_CALLBACK_QUERY_ID = "534241"
        const val ANY_CALLBACK_ANSWER_TEXT = "answer test"
        const val ANY_ANSWER_CALLBACK_URL = "http://www.telegram.com"
        const val ANY_CALLBACK_ANSWER_CACHE_TIME = 342
        const val CALLBACK_ANSWER_SHOW_ALERT = true
    }
}
