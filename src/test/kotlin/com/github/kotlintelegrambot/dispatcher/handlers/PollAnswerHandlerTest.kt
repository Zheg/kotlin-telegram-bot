package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyPollAnswer
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class PollAnswerHandlerTest {

    private val handlePollAnswerMock = mockk<HandlePollAnswer>(relaxed = true)

    private val sut = PollAnswerHandler(handlePollAnswerMock)

    @Test
    fun `checkUpdate returns false when there is no poll answer`() {
        val anyUpdateWithNoPollAnswer = anyUpdate(pollAnswer = null)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoPollAnswer)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is poll answer`() {
        val anyUpdateWithPollAnswer = anyUpdate(pollAnswer = anyPollAnswer())

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithPollAnswer)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `poll answer is properly dispatched to the handler function`() = runTest {
        val anyPollAnswer = anyPollAnswer()
        val anyUpdateWithPollAnswer = anyUpdate(pollAnswer = anyPollAnswer)

        sut.handleUpdate(anyUpdateWithPollAnswer)

        val expectedPollAnswerHandlerEnv = PollAnswerHandlerEnvironment(
            anyUpdateWithPollAnswer,
            anyPollAnswer
        )
        coVerify { handlePollAnswerMock.invoke(expectedPollAnswerHandlerEnv) }
    }
}
