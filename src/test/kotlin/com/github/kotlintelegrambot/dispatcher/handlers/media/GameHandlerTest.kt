package com.github.kotlintelegrambot.dispatcher.handlers.media

import com.github.kotlintelegrambot.anyGame
import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.dispatcher.handlers.HandleGame
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class GameHandlerTest {

    private val handleGameMock = mockk<HandleGame>(relaxed = true)

    private val sut = GameHandler(handleGameMock)

    @Test
    fun `checkUpdate returns false when there is no game`() {
        val anyUpdateWithNoGame = anyUpdate(message = anyMessage(game = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoGame)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is game`() {
        val anyUpdateWithGame = anyUpdate(message = anyMessage(game = anyGame()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithGame)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `game is properly dispatched to the handler function`() = runTest {
        val anyGame = anyGame()
        val anyMessageWithGame = anyMessage(game = anyGame)
        val anyUpdateWithGame = anyUpdate(message = anyMessageWithGame)

        sut.handleUpdate(anyUpdateWithGame)

        val expectedGameHandlerEnv = MediaHandlerEnvironment(
            anyUpdateWithGame,
            anyMessageWithGame,
            anyGame
        )
        coVerify { handleGameMock.invoke(expectedGameHandlerEnv) }
    }
}
