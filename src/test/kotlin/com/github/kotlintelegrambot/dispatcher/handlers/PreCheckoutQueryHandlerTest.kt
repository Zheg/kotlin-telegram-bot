package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.anyPreCheckoutQuery
import com.github.kotlintelegrambot.anyUpdate
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class PreCheckoutQueryHandlerTest {

    private val handlePreCheckoutQueryMock = mockk<HandlePreCheckoutQuery>(relaxed = true)

    private val sut = PreCheckoutQueryHandler(handlePreCheckoutQueryMock)

    @Test
    fun `checkUpdate returns false when there is no pre checkout query`() {
        val anyUpdateWithNoPreCheckoutQuery = anyUpdate(preCheckoutQuery = null)

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoPreCheckoutQuery)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is pre checkout query`() {
        val anyUpdateWithPreCheckoutQuery = anyUpdate(preCheckoutQuery = anyPreCheckoutQuery())

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithPreCheckoutQuery)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `pre checkout query is properly dispatched to the handler function`() = runTest {
        val anyPreCheckoutQuery = anyPreCheckoutQuery()
        val anyUpdateWithPreCheckoutQuery = anyUpdate(preCheckoutQuery = anyPreCheckoutQuery)

        sut.handleUpdate(anyUpdateWithPreCheckoutQuery)

        val expectedPreCheckoutQueryHandlerEnv = PreCheckoutQueryHandlerEnvironment(
            anyUpdateWithPreCheckoutQuery,
            anyPreCheckoutQuery
        )
        coVerify { handlePreCheckoutQueryMock.invoke(expectedPreCheckoutQueryHandlerEnv) }
    }
}
