package com.github.kotlintelegrambot.dispatcher.handlers.media

import com.github.kotlintelegrambot.anyMessage
import com.github.kotlintelegrambot.anySticker
import com.github.kotlintelegrambot.anyUpdate
import com.github.kotlintelegrambot.dispatcher.handlers.HandleSticker
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class StickerHandlerTest {

    private val handleStickerMock = mockk<HandleSticker>(relaxed = true)

    private val sut = StickerHandler(handleStickerMock)

    @Test
    fun `checkUpdate returns false when there is no sticker`() {
        val anyUpdateWithNoSticker = anyUpdate(message = anyMessage(sticker = null))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithNoSticker)

        assertFalse(checkUpdateResult)
    }

    @Test
    fun `checkUpdate returns true when there is sticker`() {
        val anyUpdateWithSticker = anyUpdate(message = anyMessage(sticker = anySticker()))

        val checkUpdateResult = sut.checkUpdate(anyUpdateWithSticker)

        assertTrue(checkUpdateResult)
    }

    @Test
    fun `sticker is properly dispatched to the handler function`() = runTest {
        val anySticker = anySticker()
        val anyMessageWithSticker = anyMessage(sticker = anySticker)
        val anyUpdateWithSticker = anyUpdate(message = anyMessageWithSticker)

        sut.handleUpdate(anyUpdateWithSticker)

        val expectedStickerHandlerEnv = MediaHandlerEnvironment(
            anyUpdateWithSticker,
            anyMessageWithSticker,
            anySticker
        )
        coVerify { handleStickerMock.invoke(expectedStickerHandlerEnv) }
    }
}
