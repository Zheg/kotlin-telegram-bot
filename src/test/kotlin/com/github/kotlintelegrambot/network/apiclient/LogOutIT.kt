package com.github.kotlintelegrambot.network.apiclient

import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LogOutIT : TelegramClientIT() {

    @Test
    fun `logOut response is correctly returned`(): Unit = runBlocking {
        givenAnyLogOutResponse()

        val logOutResponse = sut.logOut()

        assertThat(logOutResponse.getOrNull()).isTrue
    }

    private fun givenAnyLogOutResponse() {
        val logOutResponse = """
            {
                "ok": true,
                "result": true
            }
        """.trimIndent()
        val mockedResponse = MockResponse()
            .setResponseCode(200)
            .setBody(logOutResponse)
        mockWebServer.enqueue(mockedResponse)
    }
}
