package com.github.kotlintelegrambot.network.apiclient

import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.network.TelegramClient
import com.github.kotlintelegrambot.network.serialization.GsonFactory
import okhttp3.mockwebserver.MockWebServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach

abstract class TelegramClientIT {

    protected val mockWebServer = MockWebServer()

    internal lateinit var sut: TelegramClient

    @BeforeEach
    fun setUp() {
        mockWebServer.start()
        val webServerUrl = mockWebServer.url("")
        sut = TelegramClient(
            token = "",
            apiUrl = webServerUrl.toString(),
            logLevel = LogLevel.None,
            gson = GsonFactory.createForApiClient()
        )
    }

    @AfterEach
    fun tearDown() {
        mockWebServer.shutdown()
    }
}
