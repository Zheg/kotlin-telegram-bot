package com.github.kotlintelegrambot.updater

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import kotlin.random.Random

@ExperimentalCoroutinesApi
class CoroutineLooperTest {

    private fun createCoroutineLooper(coroutineDispatcher: CoroutineDispatcher) =
        CoroutineLooper(coroutineDispatcher)

    @Test
    fun `loops until quit is called`() = runTest {
        val sut = createCoroutineLooper(StandardTestDispatcher(testScheduler))
        var count = 0
        val expectedCount: Int = Random.nextInt(1000)

        sut.loop {
            count++

            if (count == expectedCount) {
                sut.quit()
            }
        }
        advanceUntilIdle()

        assertEquals(expectedCount, count)
    }

    @Test
    fun `loops until an exception is thrown by default`() = runTest {
        val sut = createCoroutineLooper(StandardTestDispatcher(testScheduler))
        var count = 0
        val expectedCount: Int = Random.nextInt(1000)

        try {
            sut.loop {
                count++

                if (count == expectedCount) {
                    throw RuntimeException("oops")
                }
            }
        } catch (testException: RuntimeException) {
        } finally {
            advanceUntilIdle()
            assertEquals(expectedCount, count)
        }
    }

    @Test
    fun `loops until quit even if an exception is thrown`() = runTest {
        val sut = CoroutineLooper(StandardTestDispatcher(testScheduler), onThrow = {})
        var count = 0
        val expectedCount: Int = Random.nextInt(1000)

        sut.loop {
            count++

            if (count == expectedCount) {
                sut.quit()
            }

            throw RuntimeException("oops")
        }

        advanceUntilIdle()
        assertEquals(expectedCount, count)
    }
}
