package com.github.kotlintelegrambot.dispatcher

import com.github.kotlintelegrambot.dispatcher.handlers.ErrorHandler
import com.github.kotlintelegrambot.dispatcher.handlers.HandleThrowable
import com.github.kotlintelegrambot.dispatcher.handlers.Handler
import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.errors.TelegramError
import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.types.DispatchableObject
import com.github.kotlintelegrambot.updater.CoroutineLooper
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import mu.KLogging
import java.util.concurrent.ConcurrentHashMap

class Dispatcher internal constructor(
    private val newUsers: Channel<Long>,
    private val userUpdatesChannels: ConcurrentHashMap<Long, Channel<DispatchableObject>>,
    private val notUserUpdatesChannel: Channel<DispatchableObject>,
    private val logLevel: LogLevel,
    updatesCheckDispatcher: CoroutineDispatcher,
    private val processingDispatcher: CoroutineDispatcher,
    private val commonThrowableHandler: HandleThrowable,
) {
    private val commandHandlers = linkedSetOf<Handler>()
    private val errorHandlers = arrayListOf<ErrorHandler>()

    private companion object : KLogging()

    private val userUpdatesCheckLooper = CoroutineLooper(updatesCheckDispatcher) { ex ->
        logger.error(ex) { "There is an exception on getting user updates" }
    }
    private val notUserUpdatesCheckLooper = CoroutineLooper(updatesCheckDispatcher) { ex ->
        logger.error(ex) { "There is an exception on getting or processing not user updates" }
    }
    private val userProcessingLoopers = ConcurrentHashMap<Long, CoroutineLooper>()
    private val processingScope = CoroutineScope(processingDispatcher)

    internal fun startCheckingUpdates() {
        userUpdatesCheckLooper.loop {
            logger.info { "start receiving new users for launching user's processing queue" }
            val userId = newUsers.receive()
            logger.info { "received new user $userId for launching user's processing queue" }
            logger.info { "launch user $userId processing queue" }
            launchProcessing(userId)
        }

        notUserUpdatesCheckLooper.loop {
            logger.info { "start receiving not users update" }
            val item = notUserUpdatesChannel.receive()
            logger.info { "received not users update class = " + item::class.simpleName }
            processingScope.launch {
                when (item) {
                    is Update -> handleUpdate(item)
                    is TelegramError -> handleError(item)
                    else -> Unit
                }
            }
        }
    }

    private suspend fun launchProcessing(userId: Long) {
        if (userProcessingLoopers.containsKey(userId)) {
            logger.warn { "user $userId processing looper already exists" }
            return
        }

        val updates = userUpdatesChannels[userId]

        if (updates == null) {
            logger.warn { "failed to launch processing for user $userId, cause updates is null" }
            newUsers.send(userId)
            return
        }

        val userLooper = CoroutineLooper(processingDispatcher) { ex ->
            logger.error(ex) { "There is an exception on processing user $userId updates" }
        }
        val existing = userProcessingLoopers.putIfAbsent(userId, userLooper)

        if (existing != null) {
            logger.warn { "found existing user $userId processing looper on insertion" }
            return
        }

        userLooper.loop {
            when (val item = updates.receive()) {
                is Update -> handleUpdate(item)
                is TelegramError -> handleError(item)

                else -> Unit
            }
        }

        logger.info { "user $userId looper has been set up" }
    }

    fun addHandler(handler: Handler) {
        commandHandlers.add(handler)
    }

    fun removeHandler(handler: Handler) {
        commandHandlers.remove(handler)
    }

    fun addErrorHandler(errorHandler: ErrorHandler) {
        errorHandlers.add(errorHandler)
    }

    fun removeErrorHandler(errorHandler: ErrorHandler) {
        errorHandlers.remove(errorHandler)
    }

    suspend fun handleUpdate(update: Update) {
        commandHandlers.filter { it.checkUpdate(update) }
            .forEach {
                if (update.consumed) {
                    return
                }
                try {
                    it.handleUpdate(update)
                } catch (throwable: Throwable) {
                    if (logLevel.shouldLogErrors()) {
                        throwable.printStackTrace()
                    }
                    try {
                        commonThrowableHandler(throwable, update)
                    } catch (throwable: Throwable) {
                        if (logLevel.shouldLogErrors()) {
                            throwable.printStackTrace()
                        }
                    }
                }
            }
    }

    private fun handleError(error: TelegramError) {
        errorHandlers.forEach { handleError ->
            try {
                handleError(error)
            } catch (throwable: Throwable) {
                if (logLevel.shouldLogErrors()) {
                    throwable.printStackTrace()
                }
            }
        }
    }

    internal fun stopCheckingUpdates() {
        userUpdatesCheckLooper.quit()
        userProcessingLoopers.forEach { (_, looper) ->
            looper.quit()
        }
        notUserUpdatesCheckLooper.quit()
    }
}
