package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.ChatMemberUpdated
import com.github.kotlintelegrambot.entities.Update

data class MyChatMemberHandlerEnvironment(
    val update: Update,
    val chatMemberUpdated: ChatMemberUpdated,
)

internal class MyChatMemberHandler(
    private val chatType: String? = null,
    private val handleMyChatMember: HandleMyChatMember,
) : Handler {

    override fun checkUpdate(update: Update): Boolean {
        return update.myChatMember != null && (chatType == null || update.myChatMember.chat.type == chatType)
    }

    override suspend fun handleUpdate(update: Update) {
        checkNotNull(update.myChatMember)
        val myChatMemberHandlerEnv = MyChatMemberHandlerEnvironment(update, update.myChatMember)
        handleMyChatMember(myChatMemberHandlerEnv)
    }
}
