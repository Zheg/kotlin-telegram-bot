package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.entities.polls.PollAnswer

data class PollAnswerHandlerEnvironment(
    val update: Update,
    val pollAnswer: PollAnswer,
)

internal class PollAnswerHandler(
    private val handlePollAnswer: HandlePollAnswer,
) : Handler {

    override fun checkUpdate(update: Update): Boolean = update.pollAnswer != null

    override suspend fun handleUpdate(update: Update) {
        val pollAnswer = update.pollAnswer
        checkNotNull(pollAnswer)

        val pollAnswerHandlerEnv = PollAnswerHandlerEnvironment(update, pollAnswer)
        handlePollAnswer(pollAnswerHandlerEnv)
    }
}
