package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.errors.TelegramError

data class ErrorHandlerEnvironment(
    val error: TelegramError,
)

class ErrorHandler(private val handler: HandleError) {

    operator fun invoke(error: TelegramError) {
        val errorHandlerEnvironment = ErrorHandlerEnvironment(error)
        handler.invoke(errorHandlerEnvironment)
    }
}
