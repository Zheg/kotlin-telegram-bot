package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.entities.Update

data class TextHandlerEnvironment(
    val update: Update,
    val message: Message,
    val text: String,
)

internal class TextHandler(
    private val text: String? = null,
    private val handleText: HandleText,
) : Handler {

    override fun checkUpdate(update: Update): Boolean {
        if (update.message?.text != null && text == null) return true
        else if (text != null) {
            return update.message?.text != null && update.message.text.contains(text, ignoreCase = true)
        }
        return false
    }

    override suspend fun handleUpdate(update: Update) {
        checkNotNull(update.message)
        checkNotNull(update.message.text)
        val textHandlerEnv = TextHandlerEnvironment(
            update,
            update.message,
            update.message.text
        )
        handleText(textHandlerEnv)
    }
}
