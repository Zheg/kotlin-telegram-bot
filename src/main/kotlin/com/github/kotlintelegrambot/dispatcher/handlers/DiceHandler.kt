package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.entities.dice.Dice

data class DiceHandlerEnvironment(
    val update: Update,
    val message: Message,
    val dice: Dice,
)

internal class DiceHandler(
    private val handleDice: HandleDice,
) : Handler {

    override fun checkUpdate(update: Update): Boolean = update.message?.dice != null

    override suspend fun handleUpdate(update: Update) {
        val message = update.message
        val dice = message?.dice
        checkNotNull(dice)

        val diceHandlerEnv = DiceHandlerEnvironment(update, message, dice)
        handleDice.invoke(diceHandlerEnv)
    }
}
