package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.Update

data class CommonHandlerEnvironment internal constructor(
    val update: Update,
)

internal class CommonUpdateHandler(
    private val handleUpdate: HandleUpdate,
) : Handler {
    override fun checkUpdate(update: Update) = true

    override suspend fun handleUpdate(update: Update) {
        handleUpdate(CommonHandlerEnvironment(update))
    }
}
