package com.github.kotlintelegrambot.dispatcher.handlers

import com.github.kotlintelegrambot.entities.CallbackQuery
import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.network.TelegramClient

data class CallbackQueryHandlerEnvironment(
    val update: Update,
    val callbackQuery: CallbackQuery,
)

data class CallbackQueryResponse(
    val text: String? = null,
    val showAlert: Boolean? = null,
    val url: String? = null,
    val cacheTime: Int? = null,
)

internal class CallbackQueryHandler(
    private val telegramClient: TelegramClient,
    private val callbackData: String? = null,
    private val handleCallbackQuery: HandleCallbackQuery,
) : Handler {

    override fun checkUpdate(update: Update): Boolean {
        val data = update.callbackQuery?.data
        return when {
            data == null -> false
            callbackData == null -> true
            else -> data.split(' ').firstOrNull().equals(callbackData, ignoreCase = true)
        }
    }

    override suspend fun handleUpdate(update: Update) {
        checkNotNull(update.callbackQuery)
        val callbackQueryHandlerEnv = CallbackQueryHandlerEnvironment(
            update,
            update.callbackQuery
        )
        val response = handleCallbackQuery(callbackQueryHandlerEnv)

        telegramClient.answerCallbackQuery(
            callbackQueryId = update.callbackQuery.id,
            text = response?.text,
            showAlert = response?.showAlert,
            url = response?.url,
            cacheTime = response?.cacheTime,
        )
    }
}
