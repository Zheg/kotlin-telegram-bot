package com.github.kotlintelegrambot.updater

import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.errors.RetrieveUpdatesError
import com.github.kotlintelegrambot.network.TelegramClient
import com.github.kotlintelegrambot.types.DispatchableObject
import com.github.kotlintelegrambot.types.TelegramBotResult
import com.github.kotlintelegrambot.webhook.WebhookConfig
import kotlinx.coroutines.channels.Channel
import mu.KLogging
import java.util.concurrent.ConcurrentHashMap

internal class Updater(
    private val looper: Looper,
    private val newUsers: Channel<Long>,
    private val userUpdatesChannels: ConcurrentHashMap<Long, Channel<DispatchableObject>>,
    private val notUserUpdatesChannel: Channel<DispatchableObject>,
    private val telegramClient: TelegramClient,
    private val botTimeout: Int,
    private val webhookConfig: WebhookConfig?,
) {

    private companion object : KLogging()

    private val limit = 10

    @Volatile
    private var lastUpdateId: Long? = -limit.toLong()

    internal fun startPolling() {
        looper.loop {
            val getUpdatesResult = telegramClient.getUpdates(
                offset = lastUpdateId,
                limit = limit,
                timeout = botTimeout,
                allowedUpdates = null,
            )

            getUpdatesResult.fold(
                ifSuccess = { updates -> onUpdatesReceived(updates) },
                ifError = { updates -> onErrorGettingUpdates(updates) },
            )
        }
    }

    internal fun stopPolling() {
        looper.quit()
    }

    /**
     * Starts a webhook through the setWebhook Telegram's API operation and starts checking
     * updates if it was successfully set.
     * @return true if the webhook was successfully set or false otherwise
     */
    suspend fun startWebhook(): Boolean {
        if (webhookConfig == null) {
            error("To start a webhook you need to configure it on bot set up. Check the `webhook` builder function")
        }

        val setWebhookResult = telegramClient.setWebhook(
            webhookConfig.url,
            webhookConfig.certificate,
            webhookConfig.ipAddress,
            webhookConfig.maxConnections,
            webhookConfig.allowedUpdates,
            webhookConfig.dropPendingUpdates
        )

        return setWebhookResult.fold(
            ifSuccess = { true },
            ifError = { false }
        )
    }

    /**
     * Deletes a webhook through the deleteWebhook Telegram's API operation and stops checking updates.
     * @return true if the webhook was successfully deleted or false otherwise
     */
    suspend fun deleteWebhook(): Boolean {
        if (webhookConfig == null) {
            error("To stop a webhook you need to configure it on bot set up. Check the `webhook` builder function")
        }

        val deleteWebhookResult = telegramClient.deleteWebhook()

        return deleteWebhookResult.fold(
            ifSuccess = { true },
            ifError = { false },
        )
    }

    private suspend fun onUpdatesReceived(updates: List<Update>) {
        if (updates.isEmpty()) {
            return
        }

        updates.forEach { update ->
            val userId = update.getUserId()
            if (userId == null) {
                notUserUpdatesChannel.send(update)
            } else {
                if (!userUpdatesChannels.containsKey(userId)) {
                    newUsers.send(userId)
                }
                userUpdatesChannels.getOrPut(userId) { Channel() }.send(update)
            }
        }

        lastUpdateId = updates.last().updateId + 1
    }

    private fun Update.getUserId(): Long? {
        return callbackQuery?.from?.id ?: message?.from?.id
    }

    private suspend fun onErrorGettingUpdates(error: TelegramBotResult.Error<List<Update>>) {
        logger.error { "Error on getting updates:\n$error" }
        val errorDescription: String? = when (error) {
            is TelegramBotResult.Error.HttpError -> "${error.httpCode} ${error.description}"
            is TelegramBotResult.Error.TelegramApi -> "${error.errorCode} ${error.description}"
            is TelegramBotResult.Error.InvalidResponse -> "${error.httpCode} ${error.httpStatusMessage}"
            is TelegramBotResult.Error.Unknown -> error.exception.message
        }

        val dispatchableError = RetrieveUpdatesError(
            errorDescription ?: "Error retrieving updates"
        )
        notUserUpdatesChannel.send(dispatchableError)
    }
}
