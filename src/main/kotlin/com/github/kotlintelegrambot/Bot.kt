package com.github.kotlintelegrambot

import com.github.kotlintelegrambot.dispatcher.Dispatcher
import com.github.kotlintelegrambot.dispatcher.handlers.CommonUpdateHandler
import com.github.kotlintelegrambot.dispatcher.handlers.HandleThrowable
import com.github.kotlintelegrambot.dispatcher.handlers.HandleUpdate
import com.github.kotlintelegrambot.entities.Update
import com.github.kotlintelegrambot.logging.LogLevel
import com.github.kotlintelegrambot.network.TelegramClient
import com.github.kotlintelegrambot.types.DispatchableObject
import com.github.kotlintelegrambot.updater.CoroutineLooper
import com.github.kotlintelegrambot.updater.Updater
import com.github.kotlintelegrambot.webhook.WebhookConfig
import com.github.kotlintelegrambot.webhook.WebhookConfigBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.Channel
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors

fun bot(body: Bot.Builder.() -> Unit): Bot = Bot.Builder().build(body)

fun Bot.Builder.dispatch(
    preHandle: HandleUpdate = { },
    commonThrowableHandler: HandleThrowable = { _, _ -> },
    commonUpdateHandler: HandleUpdate = { },
    body: Dispatcher.() -> Unit,
) {
    dispatcherConfiguration = body
    this.preHandler = preHandle
    this.commonThrowableHandler = commonThrowableHandler
    this.commonUpdateHandler = commonUpdateHandler
}

fun Bot.Builder.webhook(
    body: WebhookConfigBuilder.() -> Unit,
) {
    val webhookConfigBuilder = WebhookConfigBuilder()
    webhookConfigBuilder.apply(body)
    webhookConfig = webhookConfigBuilder.build()
}

open class Bot private constructor(
    private val updater: Updater,
    private val dispatcher: Dispatcher,
) {

    class Builder {
        var webhookConfig: WebhookConfig? = null
        lateinit var telegramClient: TelegramClient
        var timeout: Int = 30
        var logLevel: LogLevel = LogLevel.None
        internal var commonThrowableHandler: HandleThrowable = { _, _ -> }
        internal var commonUpdateHandler: HandleUpdate = { }
        internal var preHandler: HandleUpdate = { }
        internal var dispatcherConfiguration: Dispatcher.() -> Unit = { }

        private fun build(): Bot {
            val newUsers = Channel<Long>()
            val userUpdatesChannels = ConcurrentHashMap<Long, Channel<DispatchableObject>>()
            val notUserUpdatesChannel = Channel<DispatchableObject>()
            val looper = CoroutineLooper(Dispatchers.IO)
            val updater = Updater(
                looper = looper,
                newUsers = newUsers,
                userUpdatesChannels = userUpdatesChannels,
                notUserUpdatesChannel = notUserUpdatesChannel,
                telegramClient = telegramClient,
                botTimeout = timeout,
                webhookConfig = webhookConfig
            )
            val dispatcher = Dispatcher(
                newUsers = newUsers,
                userUpdatesChannels = userUpdatesChannels,
                notUserUpdatesChannel = notUserUpdatesChannel,
                logLevel = logLevel,
                updatesCheckDispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher(),
                processingDispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher(),
                commonThrowableHandler = commonThrowableHandler,
            )
                .apply { addHandler(CommonUpdateHandler(preHandler)) }
                .apply(dispatcherConfiguration)
                .apply { addHandler(CommonUpdateHandler(commonUpdateHandler)) }

            return Bot(
                updater,
                dispatcher,
            )
        }

        fun build(body: Builder.() -> Unit): Bot {
            body()
            return build()
        }
    }

    suspend fun handleUpdate(update: Update) {
        dispatcher.handleUpdate(update)
    }

    fun startPolling() {
        dispatcher.startCheckingUpdates()
        updater.startPolling()
    }

    fun stopPolling() {
        updater.stopPolling()
        dispatcher.stopCheckingUpdates()
    }

    /**
     * Starts a webhook through the setWebhook Telegram's API operation and starts checking
     * updates if it was successfully set.
     * @return true if the webhook was successfully set or false otherwise
     */
    suspend fun startWebhook(): Boolean {
        val webhookSet = updater.startWebhook()
        if (webhookSet) {
            dispatcher.startCheckingUpdates()
        }
        return webhookSet
    }

    /**
     * Deletes a webhook through the deleteWebhook Telegram's API operation and stops checking updates.
     * @return true if the webhook was successfully deleted or false otherwise
     */
    suspend fun stopWebhook(): Boolean {
        dispatcher.stopCheckingUpdates()

        return updater.deleteWebhook()
    }
}
