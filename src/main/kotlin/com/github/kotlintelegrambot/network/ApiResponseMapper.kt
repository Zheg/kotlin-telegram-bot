package com.github.kotlintelegrambot.network

import com.github.kotlintelegrambot.types.TelegramBotResult
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

internal object ApiResponseMapper {

    fun <T> CallResponse<Response<T>>.toTelegramBotResult(): TelegramBotResult<T> {
        fun invalidResponse(): TelegramBotResult.Error<T> = TelegramBotResult.Error.InvalidResponse(
            code(),
            message(),
            body()
        )

        fun Response<T>.getTelegramErrorOrInvalidResponse(): TelegramBotResult.Error<T> {
            return TelegramBotResult.Error.TelegramApi(
                errorCode = this.errorCode ?: return invalidResponse(),
                description = this.errorDescription ?: return invalidResponse()
            )
        }

        if (isSuccessful) {
            val responseBody = body() ?: return invalidResponse()
            return if (responseBody.ok) {
                val telegramResult = responseBody.result ?: return invalidResponse()

                TelegramBotResult.Success(telegramResult)
            } else {
                responseBody.getTelegramErrorOrInvalidResponse()
            }
        }

        val responseBodyString = errorBody()?.string() ?: return invalidResponse()

        val responseBody = try {
            val type = object : TypeToken<Response<T>>() {}.type
            val responseBody = Gson().fromJson<Response<T>>(responseBodyString, type)
            if (responseBody.ok) return invalidResponse() else responseBody
        } catch (e: Exception) {
            return TelegramBotResult.Error.HttpError(
                code(),
                responseBodyString
            )
        }

        return responseBody.getTelegramErrorOrInvalidResponse()
    }
}
